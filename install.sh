#!/bin/bash

CONF="/home/dwels/.config/"
THIS="/home/dwels/.dotfiles/conf_files/"

# alacritty
ln -sf ${THIS}alacritty/alacritty.yml ${CONF}alacritty/alacritty.yml

# bspwm
ln -sf ${THIS}bspwm/bspwmrc ${CONF}bspwm/bspwmrc

# dunst
ln -sf ${THIS}dunst/dunstrc ${CONF}dunst/dunstrc
ln -sf ${THIS}dunst/reload ${CONF}dunst/reload

# gitui
ln -sf ${THIS}gitui/ ${CONF}

# neofetch
ln -sf ${THIS}neofetch/config.conf ${CONF}neofetch/config.conf

# nnn
ln -sf ${THIS}nnn/ ${CONF}

# polybar
ln -sf ${THIS}polybar/config ${CONF}polybar/config
ln -sf ${THIS}polybar/launch.sh ${CONF}polybar/launch.sh

# rofi
ln -sf ${THIS}rofi/ ${CONF}

# sxhkd
ln -sf ${THIS}sxhkd/sxhkdrc ${CONF}sxhkd/sxhkdrc

