#!/bin/bash

## Author  : Aditya Shakya
## Mail    : adi1090x@gmail.com
## Github  : @adi1090x
## Twitter : @adi1090x

# Available Styles
# >> Created and tested on : rofi 1.6.0-1
#
# style_2
# style_7     style_8     style_9

theme="style_7"
dir="$HOME/.config/rofi"

# dark
ALPHA="#00000000"
BG="#000000ff"
FG="#FFFFFFff"
SELECT="#101010ff"

# light
#ALPHA="#00000000"
#BG="#FFFFFFff"
#FG="#000000ff"
#SELECT="#f3f3f3ff"

# accent colors
COLORS=('#42A5F5' '#00B19F')

ACCENT="${COLORS[$(( $RANDOM % 1 ))]}ff"

# overwrite colors file
cat > $dir/colors.rasi <<- EOF
	/* colors */
	* {
	  al:  $ALPHA;
	  bg:  $BG;
	  se:  $SELECT;
	  fg:  $FG;
	  ac:  $ACCENT;
	}
EOF


#rofi -no-lazy-grab -show drun -theme $dir/"$theme"
rofi -show drun -modi drun -theme $dir/"$theme"
